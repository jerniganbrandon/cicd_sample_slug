# Continuous Integration and Deployment Sample Application

### System Requirements
- Java version 8
- Ant version 1.10.12
- PostgreSQL version 14.2
- JUnit4 version 4.13.2
- Mockito version 1.10.19
- JDBC PostgreSQL Driver version 42.3.3

### Set Up the Database
Ensure that you have a PostgreSQL server running on port 5432 and a database named [cidsa]. If you need to populate the database with sample data, use the [seed.sql] file under the database directory.

```java
// From the command line, at the project root "/cicd_sample_slug/cidsa/"
// Only if you need to seed the database
$ psql -U [postgres username] -d cidsa -f database\seed.sql
```

### Building the Application
```java
// From the command line, at the project root
$ ant all
```

### Running the Application
```java
// From the command line, at the project root
$ cd dist
$ java -jar CIDSA.jar [postgres username] [postgres password]
```