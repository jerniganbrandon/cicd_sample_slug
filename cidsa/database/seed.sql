DROP TABLE IF EXISTS points;

CREATE TABLE points (
    id SERIAL PRIMARY KEY,
    name VARCHAR(15),
    level INT,
    x INT,
    y INT
);

INSERT INTO points (name, level, x, y) VALUES ('SIA 1', 3, 100, 230);
INSERT INTO points (name, level, x, y) VALUES ('SIA 2', 1, 34, 85);
INSERT INTO points (name, level, x, y) VALUES ('SIA 3', 3, 701, 74);
INSERT INTO points (name, level, x, y) VALUES ('SIA 4', 2, 70, 400);
INSERT INTO points (name, level, x, y) VALUES ('SIA 5', 3, 500, 400);
INSERT INTO points (name, level, x, y) VALUES ('SIA 6', 3, 20, 345);
INSERT INTO points (name, level, x, y) VALUES ('SIA 7', 2, 80, 87);
INSERT INTO points (name, level, x, y) VALUES ('SIA 8', 3, 10, 10);
INSERT INTO points (name, level, x, y) VALUES ('SIA 9', 3, 500, 500);
