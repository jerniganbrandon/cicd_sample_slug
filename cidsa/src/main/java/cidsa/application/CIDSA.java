package main.java.cidsa.application;
/***********************************************************
 * Continuous Integration and Deployment Sample Application
 * 
 * Original Author: Brandon Jernigan
 * Date Written: 14 March 2022
 **********************************************************/

/**
 * Change Log:
 * -----------
 * 14 March 2022 | Brandon Jernigan | Baseline Complete
 * 18 March 2022 | Brandon Jernigan | Added fatJar task to gradle build
 * 16 May   2022 | Brandon Jernigan | Changed to Ant, updated package names, added mockito to tests
 */

import java.awt.Color;
import java.awt.Cursor;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
import javax.swing.JLayeredPane;

import main.java.cidsa.models.Point;
import main.java.cidsa.views.MapPoint;
import main.java.cidsa.services.Listener;
import main.java.cidsa.injector.Injector;
import main.java.cidsa.services.Refreshable;
import main.java.cidsa.services.DataManager;
import main.java.cidsa.services.NotificationManager;

enum Mode {Normal, Add};

/**
 * This is the primary class for the CIDSA; containing the main method 
 * and controlling the two portions of the application, CIDSA and Injector.
 */
public class CIDSA extends JFrame implements MouseListener, Refreshable, Listener {
    private int WINDOW_WIDTH = 1280;
    private int WINDOW_HEIGHT = 720;

    public DataManager dataManager;

    private JLayeredPane layerPane;
    private JLayeredPane overlay;
    private Mode mode;

    private String puser;
    private String ppassword;
    
    public CIDSA(String puser, String ppassword) throws IOException {
        this.puser = puser;
        this.mode = Mode.Normal;
        this.ppassword = ppassword;
        this.dataManager = new DataManager(puser, ppassword);
        NotificationManager.getInstance().addListener(this);


        layerPane = new JLayeredPane();
        layerPane.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        this.add(layerPane);

        this.setFrameProperties();
        this.createButtons();
        this.createMap();
        this.loadPoints();
    }

    private void setFrameProperties() {
        this.setVisible(true);
        this.setTitle("CIDSA");
        this.setResizable(false);
        this.addMouseListener(this);
        this.setCursor(Cursor.DEFAULT_CURSOR);
        this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * This method creates a "map" which is comprised of the 
     * background image (JLabel containing an ImageIcon) and
     * the overlay (JLayeredPane containing all graphics).
     * 
     * @throws IOException
     */
    private void createMap() throws IOException {
        overlay = new JLayeredPane();
        overlay.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        layerPane.add(overlay, JLayeredPane.MODAL_LAYER);

        BufferedImage image = ImageIO.read(
                getClass().getClassLoader().getResource("Map.jpg")
            );
        
        ImageIcon icon = new ImageIcon(image);
        JLabel background = new JLabel(icon);
        background.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        layerPane.add(background, JLayeredPane.DEFAULT_LAYER);
    }

    private void createButtons() {
        JButton addButton = buttonFactory("Add", 5);
        addButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                mode = Mode.Add;
                setCursor(Cursor.CROSSHAIR_CURSOR);
            }
        });

        JButton populateButton = buttonFactory("Populate", 55);
        populateButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                mode = Mode.Normal;
                setCursor(Cursor.DEFAULT_CURSOR);
                new Injector(puser, ppassword);
            }
        });
            
        layerPane.add(addButton, JLayeredPane.MODAL_LAYER);
        layerPane.add(populateButton, JLayeredPane.MODAL_LAYER);
    }

    private JButton buttonFactory(String text, int verticalPosition) {
        JButton button = new JButton(text);
        button.setBackground(new Color(0.0f, 0.0f, 0.0f, 0.95f));
        button.setForeground(Color.WHITE);
        button.setBorderPainted(false);
        button.setBounds(5, verticalPosition, 100, 40);
        return button;
    }

    private void loadPoints() throws IOException {
        ArrayList<Point> points = dataManager.getAllPoints();
        for (Point point : points) {
            MapPoint mapPoint = new MapPoint(point, dataManager, this);
            overlay.add(mapPoint.icon);
        }
    }

    private void showAddOption(MouseEvent e) throws IOException {
        JPanel panel = new JPanel();
        JLabel nameLabel = new JLabel("Name: ");
        JTextField nameField = new JTextField("name");
        nameField.setColumns(10);

        JLabel levelLabel = new JLabel("Level: ");
        Integer[] levelOptions = {1, 2, 3};
        JComboBox<Integer> levelField = new JComboBox<>(levelOptions);
        
        panel.add(nameLabel);
        panel.add(nameField);
        panel.add(levelLabel);
        panel.add(levelField);

        int result = JOptionPane.showConfirmDialog(
            this, panel, "Add Point", 
            JOptionPane.OK_CANCEL_OPTION, 
            JOptionPane.PLAIN_MESSAGE
        );

        if (result == JOptionPane.OK_OPTION) {
            Point point = new Point(
                nameField.getText(), 
                levelField.getSelectedIndex()+1, 
                e.getX(), e.getY()
            );
            dataManager.addPoint(point);
            MapPoint mapPoint = new MapPoint(point, dataManager, this);
            this.overlay.add(mapPoint.icon);
        }

        mode = Mode.Normal;
        setCursor(Cursor.DEFAULT_CURSOR);
    }

    @Override
    public void refresh() throws IOException {
        overlay.removeAll();
        this.loadPoints();
        this.repaint();
    }

    @Override
    public void onNotification() throws IOException {
        this.refresh();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (mode != Mode.Add) return;
        try {
            showAddOption(e);
        } catch (IOException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}
}