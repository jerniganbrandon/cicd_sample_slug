package main.java.cidsa.application;

import java.io.IOException;

public class Launcher {
    public static void main(String[] args) throws IOException {
        new CIDSA(args[0], args[1]);
    }
}
