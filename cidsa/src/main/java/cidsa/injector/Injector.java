package main.java.cidsa.injector;

import java.util.Timer;
import java.awt.Cursor;
import java.util.Random;
import java.awt.Dimension;
import java.io.IOException;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JComboBox;

import main.java.cidsa.models.Point;
import main.java.cidsa.services.DataManager;
import main.java.cidsa.services.NotificationManager;

public class Injector extends JFrame  {
    private int WINDOW_WIDTH = 400;
    private int WINDOW_HEIGHT = 200;

    private NotificationManager notificationManager;
    private DataManager dataManager;
    private int secondsRemaining;
    private JPanel messagePanel;
    private JPanel menuPanel;
    private Random random;
    private int rate;

    public Injector(String puser, String ppassword) {
        notificationManager = NotificationManager.getInstance();
        this.dataManager = new DataManager(puser, ppassword);
        this.setFrameProperties();
        this.formatMenuPanel();
    }

    private void setFrameProperties() {
        this.setVisible(true);
        this.setTitle("CIDSA Injector");
        this.setResizable(false);
        this.setCursor(Cursor.DEFAULT_CURSOR);
        this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    private void formatMenuPanel() {
        menuPanel = new JPanel();
        menuPanel.add(Box.createRigidArea(new Dimension(WINDOW_WIDTH, 25)));

        JLabel rateLabel = new JLabel("Rate: ");
        String[] rateOptions = {
            "Every 1 second",
            "Every 5 seconds",
            "Every 10 seconds"
        };
        JComboBox<String> rateField = new JComboBox<>(rateOptions);
        rateField.setSize(300, 25);
        menuPanel.add(rateLabel);
        menuPanel.add(rateField);

        menuPanel.add(Box.createHorizontalStrut(10));

        JLabel durationLabel = new JLabel("Duration: ");
        String[] durationOptions = {
            "30 seconds",
            "60 seconds",
            "90 seconds"
        };
        JComboBox<String> durationField = new JComboBox<>(durationOptions);
        menuPanel.add(durationLabel);
        menuPanel.add(durationField);

        menuPanel.add(Box.createRigidArea(new Dimension(WINDOW_WIDTH, 25)));

        this.add(menuPanel);
        addMenuButtons(rateField, durationField);
    }

    private void addMenuButtons( JComboBox<String> rate, JComboBox<String> duration) {
        JButton startButton = new JButton("Start");
        JComboBox<String> interval = rate;
        JComboBox<String> time = duration;
        startButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                performInjection(interval.getSelectedIndex(), time.getSelectedIndex());
            }
        });
        menuPanel.add(startButton);

        menuPanel.add(Box.createHorizontalStrut(25));

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                close();
            }
        });
        menuPanel.add(cancelButton);
    }

    private void close() {
        this.dispose();
    }

    private void formatMessagePanel(JLabel remainingLabel) {
        messagePanel = new JPanel();
        messagePanel.add(Box.createRigidArea(new Dimension(WINDOW_WIDTH, 25)));

        remainingLabel.setText(
            String.format("Time remaining %d seconds", secondsRemaining)
        );
        messagePanel.add(remainingLabel);

        this.remove(menuPanel);
        this.add(messagePanel);
    }

    private void calculateRate(int rateIndex) {
        switch (rateIndex) {
            case 0:
                rate = 1;
                break;
            case 1:
                rate = 5;
                break;
            case 2:
                rate = 10;
                break;
        }
    }

    private void calculateSeconds(int durationIndex) {
        switch (durationIndex) {
            case 0:
                secondsRemaining = 30;
                break;
            case 1:
                secondsRemaining = 60;
                break;
            case 2:
                secondsRemaining = 90;
        }
    }

    private void createNewPoint() {
        String name = "Test" + secondsRemaining;
        int x = random.nextInt(1280);
        int y = random.nextInt(720);
        int level = random.nextInt(4);
        level = level == 0 ? 1 : level;

        Point point = new Point(name, level, x, y);
        int id = dataManager.addPoint(point);
        point.setId(id);
    }

    public void performInjection(int rateIndex, int durationIndex) {
        calculateRate(rateIndex);
        calculateSeconds(durationIndex);

        random = new Random();
        JLabel remainingLabel = new JLabel();
        formatMessagePanel(remainingLabel);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                if (secondsRemaining <= 0) {
                    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                    timer.cancel();
                    close();
                }
                
                if ((secondsRemaining % rate) == 0) {
                    createNewPoint();
                    try {
                        notificationManager.notifyListener();
                    } catch (IOException e) {
                        System.out.print(e.getLocalizedMessage());
                    }
                }

                secondsRemaining--;
                remainingLabel.setText(
                    String.format("Time remaining %d seconds", secondsRemaining)
                );
            }
            
        }, 1000, 1000);
    }
}
