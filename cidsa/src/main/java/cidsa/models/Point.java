package main.java.cidsa.models;

public class Point {
    private String name;
    private int level;
    private int id;
    private int x;
    private int y;

    public Point(String name, int level, int x, int y) {
        this.name = name;
        this.level = level;
        this.x = x;
        this.y = y;
    }

    public Point(String name, int level, int id, int x, int y) {
        this.name = name;
        this.level = level;
        this.id = id;
        this.x = x;
        this.y = y;
    }

    public String getName() { 
        return name; 
    }

    public void setName(String name){ 
        this.name = name; 
    }

    public int getLevel() { 
        return level; 
    }
    
    public void setLevel(int level) {
        this.level = level;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
