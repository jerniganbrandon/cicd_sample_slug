package main.java.cidsa.services;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import java.util.ArrayList;

import main.java.cidsa.models.Point;

public class DataManager {
    private String postgres_url;
    private String postgres_user;
    private String postgres_password;

    public DataManager(String puser, String ppassword) {
        postgres_url = "jdbc:postgresql://localhost:5432/cidsa";
        postgres_user = puser;
        postgres_password = ppassword;
    }

    public Connection openConnection() {
        try {
            Connection connection = DriverManager.getConnection(
                postgres_url,
                postgres_user,
                postgres_password
            );
            return connection;
        } catch (SQLException exception) {
            System.out.println(exception.getLocalizedMessage());
            return null;
        }
    }

    public ArrayList<Point> getAllPoints() {
        try {
            Connection connection = openConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM points");
            ResultSet results = statement.executeQuery();
            ArrayList<Point> points = new ArrayList<>();
    
            while (results.next()) {
                Point point = new Point(
                    results.getString("name"),
                    results.getInt("level"),
                    results.getInt("id"),
                    results.getInt("x"),
                    results.getInt("y")
                );
                points.add(point);
            }
            
            return points;
        } catch (SQLException exception) {
            System.out.println(exception.getLocalizedMessage());
            return null;
        }
    }

    public int addPoint(Point point) {
        try {
            Connection connection = openConnection();
            PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO points (name, level, x, y) VALUES (?, ?, ?, ?) RETURNING id"
            );
            statement.setString(1, point.getName());
            statement.setInt(2, point.getLevel());
            statement.setInt(3, point.getX());
            statement.setInt(4, point.getY());

            ResultSet result = statement.executeQuery();
            result.next();
            return result.getInt("id");
        } catch (SQLException exception) {
            System.out.println(exception.getLocalizedMessage());
            return -1;
        }
    }

    public void deletePoint(int id) {
        try {
            Connection connection = openConnection();
            PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM points WHERE id = ?"
            );
            statement.setInt(1, id);
            statement.executeUpdate();
            return;
        } catch (SQLException exception) {
            System.out.println("This is the fail point");
            System.out.println(exception.getLocalizedMessage());
        }
    }
}
