package main.java.cidsa.services;

import java.io.IOException;

public interface Listener {
    void onNotification() throws IOException;
}
