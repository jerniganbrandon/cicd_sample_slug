package main.java.cidsa.services;

import java.io.IOException;
import java.util.ArrayList;

public class NotificationManager {

    private static NotificationManager instance = new NotificationManager();
    private ArrayList<Listener> listeners = new ArrayList<>();

    private NotificationManager(){}

    public static NotificationManager getInstance() {
        return instance;
    }

    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    public void notifyListener() throws IOException {
        if (listeners.size() < 1) return;

        for (Listener listener : listeners) {
            listener.onNotification();
        }
    }
}
