package main.java.cidsa.services;

import java.io.IOException;
public interface Refreshable {
    void refresh() throws IOException;
}
