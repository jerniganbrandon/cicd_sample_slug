package main.java.cidsa.views;

import java.io.IOException;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import main.java.cidsa.models.Point;
import main.java.cidsa.services.DataManager;
import main.java.cidsa.services.Refreshable;

public class MapPoint implements MouseListener {
    public Point point;
    public JLabel icon;
    
    private DataManager dataManager;
    private Refreshable refreshable;

    private String htmlOpen = "<html>";
    private String htmlBreak = "<br>";
    private String htmlClose = "</html";

    public MapPoint(Point point, DataManager dataManager, 
        Refreshable refreshable) throws IOException 
    {
        this.dataManager = dataManager;
        this.refreshable = refreshable;
        this.point = point;
        this.createIcon(point.getLevel());
        this.formatToolTip();
    }

    private void createIcon(int level) throws IOException {
        String imageName = "";
        switch (level) {
            case 1:
                imageName = "One.png";
                break;
            case 2:
                imageName = "Two.png";
                break;
            case 3:
                imageName = "Three.png";
                break;
        }

        BufferedImage image = ImageIO.read(
            getClass().getClassLoader().getResource(imageName)
        );
        ImageIcon imageIcon = new ImageIcon(image);
        icon = new JLabel(imageIcon);
        icon.setSize(25, 25);
        icon.setLocation(point.getX(), point.getY());
        icon.addMouseListener(this);
        icon.setToolTipText(this.point.getName());
    }

    /**
     * Formats the text to be displayed when a mouse is hovered
     * over "this" icon.
     */
    private void formatToolTip() {
        String name = String.format("Name: %s", point.getName());
        String level = String.format("Level: %s", String.valueOf(point.getLevel()));

        this.icon.setToolTipText(
            htmlOpen +
            name +
            htmlBreak +
            level +
            htmlClose
        );
    }

    private void showDeleteOption(MouseEvent e) {
        JPanel popup = new JPanel();
        String question = "Are you sure you want to delete this point?";
        String name = String.format("Name: %s", point.getName());
        String level = String.format("Level: %s", String.valueOf(point.getLevel()));

        JLabel message = new JLabel(
            htmlOpen +
            question +
            htmlBreak +
            name +
            htmlBreak +
            level +
            htmlClose
        );
        popup.add(message);

        int result = JOptionPane.showConfirmDialog(
            icon.getRootPane(), popup, "Delete point", 
            JOptionPane.YES_NO_OPTION, 
            JOptionPane.PLAIN_MESSAGE
        );

        if (result == JOptionPane.YES_OPTION) {
            dataManager.deletePoint(point.getId());
            try {
                refreshable.refresh();
            } catch (IOException exception) {
                System.out.println(exception.getLocalizedMessage());
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        showDeleteOption(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}
}
