package test.java.cidsa;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Test;
import org.mockito.Mockito;
import static org.junit.Assert.assertEquals;

import main.java.cidsa.services.DataManager;

public class DataManagerTest {
	
	/**
	 * Spies on the DataManager class, providing a mock Connection class
	 * on call to openConnection; asserting connection is open.
	 * 
	 * @throws SQLException
	 */
	@Test
	public void openConnectionTest() throws SQLException {
		DataManager testDataManager = Mockito.spy(new DataManager("", ""));
		Connection testConnection = Mockito.mock(Connection.class);
		Mockito.when(testDataManager.openConnection()).thenReturn(testConnection);
		assertEquals(testConnection.isClosed(), false);
	}
}

