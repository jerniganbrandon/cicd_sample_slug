package test.java.cidsa;

import java.io.IOException;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import main.java.cidsa.services.Listener;
import main.java.cidsa.services.NotificationManager;

public class NotificationManagerTest implements Listener {
	private boolean notifiedFlag = false;

	/**
	 * Adds this class (implementation of Listener) to the 
	 * notification list, and asserts the notification flag
	 * changes to true.
	 * 
	 * @throws IOException
	 */
	@Test
	public void notifyListenerTest() throws IOException {
		NotificationManager testNotificationManager = NotificationManager.getInstance();
		testNotificationManager.addListener(this);
		testNotificationManager.notifyListener();
		assertEquals(notifiedFlag, true);
	}

	@Override
	public void onNotification() throws IOException {
		this.notifiedFlag = true;
	}
}
